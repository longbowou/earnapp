# [EarnApp](https://earnapp.com/i/ad5s5bc)

Earn passive income while your devices rest

## How to run it?

- Clone the repository
- Run

```bash
docker-compose up -d
```
