FROM ubuntu:latest

RUN  apt-get update \
  && apt-get install -y wget \
  && rm -rf /var/lib/apt/lists/*

RUN wget https://brightdata.com/static/earnapp/install.sh
RUN chmod +x install.sh
